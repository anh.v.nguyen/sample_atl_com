
// AutoATL.cpp : Defines the class behaviors for the application.
//

#include "pch.h"
#include "framework.h"
#include "afxwinappex.h"
#include "afxdialogex.h"
#include "AutoATL.h"
#include "MainFrm.h"

#include "ChildFrm.h"

#include "autoatl_i.c"

#include "CustApp.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

class CmfcatlappModule :
	public CAtlMfcModule
{
public:
	DECLARE_LIBID(LIBID_AutoAtl);
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_AUTOATL, "{57C20D1C-EEFE-4929-BD7E-6B467803B1CF}");
};

CmfcatlappModule _AtlModule;

// CAutoATLApp

BEGIN_MESSAGE_MAP(CAutoATLApp, CWinApp)
	ON_COMMAND(ID_APP_ABOUT, &CAutoATLApp::OnAppAbout)
	ON_COMMAND(ID_FILE_NEW, &CAutoATLApp::OnFileNew)
END_MESSAGE_MAP()


// CAutoATLApp construction

CAutoATLApp::CAutoATLApp() noexcept
{

	// TODO: replace application ID string below with unique ID string; recommended
	// format for string is CompanyName.ProductName.SubProduct.VersionInformation
	SetAppID(_T("AutoATL.AppID.NoVersion"));

	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

// The one and only CAutoATLApp object

CAutoATLApp theApp;

const GUID CDECL BASED_CODE _tlid =
{ 0x5D0CE84A, 0xD909, 0x11CF, { 0x91, 0xFC, 0x0, 0xA0, 0xC9, 0x3, 0x97, 0x6F } };
const WORD _wVerMajor = 1;
const WORD _wVerMinor = 0;

// CAutoATLApp initialization

BOOL CAutoATLApp::InitInstance()
{
	CWinApp::InitInstance();
	CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);

	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}

	AfxEnableControlContainer();

	EnableTaskbarInteraction(FALSE);

	// Parse command line for automation or reg/unreg switches.
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);
	// App was launched with /Embedding or /Automation switch.
	// Run app as automation server.
	if (cmdInfo.m_bRunEmbedded || cmdInfo.m_bRunAutomated)
	{
		// Register class factories via CoRegisterClassObject().
		if (FAILED(_AtlModule.RegisterClassObjects(CLSCTX_LOCAL_SERVER, REGCLS_MULTIPLEUSE)))
			return FALSE;
		// Register class factories via CoRegisterClassObject().
		COleTemplateServer::RegisterAll();
		// Don't show the main window
		return TRUE;
	}
	// App was launched with /Unregserver or /Unregister switch.  Remove
	// entries from the registry.
	else if (cmdInfo.m_nShellCommand == CCommandLineInfo::AppUnregister)
	{
		_AtlModule.UpdateRegistryAppId(FALSE);
		_AtlModule.UnregisterServer(TRUE);
		COleObjectFactory::UpdateRegistryAll(FALSE);
		AfxOleUnregisterTypeLib(_tlid, _wVerMajor, _wVerMinor);
		return FALSE;
	}
	// App was launched standalone or with other switches (e.g. /Register
	// or /Regserver).  Update registry entries, including typelibrary.
	else
	{
		COleObjectFactory::UpdateRegistryAll();
		_AtlModule.UpdateRegistryAppId(TRUE);
		_AtlModule.RegisterServer(TRUE);
		AfxOleRegisterTypeLib(AfxGetInstanceHandle(), _tlid);
		if (cmdInfo.m_nShellCommand == CCommandLineInfo::AppRegister)
			return FALSE;
	}

	// AfxInitRichEdit2() is required to use RichEdit control
	// AfxInitRichEdit2();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));


	// To create the main window, this code creates a new frame window
	// object and then sets it as the application's main window object
	CMDIFrameWnd* pFrame = new CMainFrame;
	if (!pFrame)
		return FALSE;
	m_pMainWnd = pFrame;
	// create main MDI frame window
	if (!pFrame->LoadFrame(IDR_MAINFRAME))
		return FALSE;
	// try to load shared MDI menus and accelerator table
	//TODO: add additional member variables and load calls for
	//	additional menu types your application may need
	HINSTANCE hInst = AfxGetResourceHandle();
	m_hMDIMenu  = ::LoadMenu(hInst, MAKEINTRESOURCE(IDR_AutoATLTYPE));
	m_hMDIAccel = ::LoadAccelerators(hInst, MAKEINTRESOURCE(IDR_AutoATLTYPE));

	{
		IAcadApplication* application = nullptr;
		IClassFactory* pIFactory = nullptr;
		auto hres = CoGetClassObject(CLSID_AcadApplication, CLSCTX_ALL, nullptr, IID_IClassFactory, (void**)&pIFactory);
		hres = pIFactory->CreateInstance(nullptr, IID_IAcadApplication, (void**)&application);
		if (SUCCEEDED(hres))
		{
			MessageBox(nullptr, L"Testing Create Inst", L"Test", MB_OK);
		}
	}

	// The main window has been initialized, so show and update it
	pFrame->ShowWindow(m_nCmdShow);
	pFrame->UpdateWindow();

	return TRUE;
}

int CAutoATLApp::ExitInstance()
{
	//TODO: handle additional resources you may have added
	if (m_hMDIMenu != nullptr)
		FreeResource(m_hMDIMenu);
	if (m_hMDIAccel != nullptr)
		FreeResource(m_hMDIAccel);

	AfxOleTerm(FALSE);

	return CWinApp::ExitInstance();
}

// CAutoATLApp message handlers

void CAutoATLApp::OnFileNew()
{
	CMainFrame* pFrame = STATIC_DOWNCAST(CMainFrame, m_pMainWnd);
	// create a new MDI child window
	pFrame->CreateNewChild(
		RUNTIME_CLASS(CChildFrame), IDR_AutoATLTYPE, m_hMDIMenu, m_hMDIAccel);
}

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg() noexcept;

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() noexcept : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()

#include "AutoATL.h"
// App command to run the dialog
void CAutoATLApp::OnAppAbout()
{
	ICustApp* app;
	auto hres = CoCreateInstance(CLSID_CustApp, nullptr, CLSCTX_ALL, IID_IUnknown, (void**)&app);
	int value = 0;
	hres = app->Func2(&value);

	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

// CAutoATLApp message handlers



