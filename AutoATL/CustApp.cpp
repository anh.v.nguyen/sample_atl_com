// Application.cpp : Implementation of CApplication

#include "pch.h"
#include "CustApp.h"

#include "../ExtAtl/ExtAtl_i.c"
#include "../../ServerATL/ServerATL_i.c"
#include "../ODA/OdaToolkit_i.c"
// CApplication

CCustApp::~CCustApp()
{
	if (m_dwRegister != 0)
		RevokeActiveObject(m_dwRegister, NULL);
}

STDMETHODIMP CCustApp::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* const arr[] = 
	{
		&IID_ICustApp
	};

	for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}


bool CCustApp::RegisterActive()
{
	IUnknown* unknown = nullptr;
	if (FAILED(QueryInterface(IID_IUnknown, (void**)&unknown)))
	{
		MessageBox(nullptr, L"Could not query interface", L"Test", MB_OK);
	}
	auto hres = RegisterActiveObject(unknown, CLSID_CustApp, NULL, &m_dwRegister);
	return SUCCEEDED(hres);
}

STDMETHODIMP CCustApp::Func2(int* value)
{
	//CComQIPtr<IExtObj, &IID_IExtObj> pIApp(GetControllingUnknown());
	//CComQIPtr<IApplication, &IID_IApplication> pIApp(GetControllingUnknown());
	CComQIPtr<IOdaHostApp3, &IID_IOdaHostApp3> pIApp(GetControllingUnknown());
	//CComQIPtr<ITestObj, &IID_ITestObj> pIApp(GetControllingUnknown());
	if (pIApp != NULL)
	{
		//pIApp->Func(value);
		//pIApp->get_Height(value);
		return S_OK;
	}

	return S_FALSE;
}

STDMETHODIMP_(HRESULT __stdcall) CCustApp::Test()
{
	MessageBox(nullptr, L"Test OK", L"Test", MB_OK);
	return S_OK;
}
