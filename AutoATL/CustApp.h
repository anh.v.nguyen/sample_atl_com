// Application.h : Declaration of the CApplication

#pragma once
#include "resource.h"       // main symbols



#include "autoatl_h.h"
#include "../ExtAtl/ExtAtl_i.h"

#include "../../ServerATL/ServerATL_i.h"

#include "../ODA/OdaX.h"
#include "../ODA/OdaToolkit.h"

#include "dll_helper.h"

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif

using namespace ATL;


// CApplication

class ATL_NO_VTABLE CCustApp :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CCustApp, &CLSID_CustApp>,
	public ISupportErrorInfo,
	public IDispatchImpl<ICustApp, &IID_ICustApp, &LIBID_AutoAtl, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	CCustApp() :
		m_dwRegister(0)
	{
	}

	~CCustApp();

DECLARE_REGISTRY_RESOURCEID(IDR_CUSTAPP)
DECLARE_GET_CONTROLLING_UNKNOWN()


BEGIN_COM_MAP(CCustApp)
	COM_INTERFACE_ENTRY(ICustApp)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY_AGGREGATE(IID_IAcadApplication, m_pApplication.p)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

	CComPtr<IUnknown> m_pApplication;

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		IUnknown* pUnkOuter = GetControllingUnknown();

		//HRESULT hRes = CoCreateInstance(CLSID_ExtObj, pUnkOuter, CLSCTX_INPROC_SERVER, IID_IUnknown, (void**)&m_pApplication);
		//HRESULT hRes = CoCreateInstance(CLSID_OdaHostApp, pUnkOuter, CLSCTX_ALL, IID_IUnknown, (void**)&m_pApplication);
		//HRESULT hRes = CoCreateInstance(CLSID_TestObj, pUnkOuter, CLSCTX_ALL, IID_IUnknown, (void**)&m_pApplication);
		auto hRes = MyCoCreateInstance(L"TeighaX_23.4src_17.dll", CLSID_AcadApplication, pUnkOuter, IID_IUnknown, (void**)&m_pApplication);
		if (hRes != S_OK)
			return hRes;

		return S_OK;
	}

	void FinalRelease()
	{
		m_pApplication.Release();
	}

	bool RegisterActive();

public:

	STDMETHOD(Func2)(int* value);
	STDMETHOD(Test)();

private:
	DWORD m_dwRegister;
};

OBJECT_ENTRY_AUTO(__uuidof(CustApp), CCustApp)
