// Application.cpp : Implementation of CApplication

#include "pch.h"
#include "Application.h"


// CApplication

STDMETHODIMP CApplication::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* const arr[] = 
	{
		&IID_IApplication
	};

	for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}


STDMETHODIMP CApplication::Func(int* value)
{
	AFX_MANAGE_STATE(AfxGetAppModuleState());

	// TODO: Add your dispatch handler code here
	*value = 12;
	return S_OK;
}
