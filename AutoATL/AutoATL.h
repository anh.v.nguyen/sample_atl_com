
// AutoATL.h : main header file for the AutoATL application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'pch.h' before including this file for PCH"
#endif

#include "autoatl_h.h"
#include "resource.h"       // main symbols


// CAutoATLApp:
// See AutoATL.cpp for the implementation of this class
//

class CAutoATLApp : public CWinApp
{
public:
	CAutoATLApp() noexcept;


// Overrides
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

// Implementation
protected:
	HMENU  m_hMDIMenu;
	HACCEL m_hMDIAccel;

public:
	afx_msg void OnAppAbout();
	afx_msg void OnFileNew();
	DECLARE_MESSAGE_MAP()
};

extern CAutoATLApp theApp;
