// dllmain.h : Declaration of module class.

class CExtAtlModule : public ATL::CAtlDllModuleT< CExtAtlModule >
{
public :
	DECLARE_LIBID(LIBID_ExtAtlLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_EXTATL, "{224fcd03-7d2a-49e0-93bf-91eee22eb9b1}")
};

extern class CExtAtlModule _AtlModule;
