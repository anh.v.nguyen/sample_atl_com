// ExtObj.h : Declaration of the CExtObj

#pragma once
#include "resource.h"       // main symbols



#include "ExtAtl_i.h"



#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif

using namespace ATL;


// CExtObj

class ATL_NO_VTABLE CExtObj :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CExtObj, &CLSID_ExtObj>,
	public ISupportErrorInfo,
	public IDispatchImpl<IExtObj, &IID_IExtObj, &LIBID_ExtAtlLib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	CExtObj()
	{
	}

DECLARE_REGISTRY_RESOURCEID(106)


BEGIN_COM_MAP(CExtObj)
	COM_INTERFACE_ENTRY(IExtObj)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:

	STDMETHOD(Func)(int* value);

};

OBJECT_ENTRY_AUTO(__uuidof(ExtObj), CExtObj)
