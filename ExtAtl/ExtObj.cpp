// ExtObj.cpp : Implementation of CExtObj

#include "pch.h"
#include "ExtObj.h"


// CExtObj

STDMETHODIMP CExtObj::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* const arr[] = 
	{
		&IID_IExtObj
	};

	for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

STDMETHODIMP_(HRESULT __stdcall) CExtObj::Func(int* value)
{
	*value = 15;
	return S_OK;
}
